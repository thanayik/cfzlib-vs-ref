import os
import sys
import time
from glob import glob
from statistics import mean
import seaborn as sns
import pandas as pd

pth = "/Users/thanayik/Dev/cfzlib-tests"
print(pth)
# file = os.path.join(pth, "big.nii.gz")
out = os.path.join(pth, "out.nii.gz")
files = glob(os.path.join(pth, "combo*.nii.gz"))
files.sort()
reps = 20

alltimes = []
allsizes = []
allzlib = []

for file in files:
    allsizes.append(os.path.getsize(file)/1000000)
    times = []
    for i in range(0, reps):
        cmd = "fslmaths {} -add 0.0 {}".format(file, out)
        # print(cmd)
        start = time.time()
        os.system(cmd)
        dur = time.time() - start  # duration in secs
        # print("gzip took: {}".format(dur))
        times.append(dur)
        os.remove(out)

    # print(alldurs)
    print("filesize: {}\ttime: {}".format(os.path.getsize(file)/1000000, mean(times)))
    alltimes.append(mean(times))
    allzlib.append('refzlib')

data = pd.DataFrame({'file_size': allsizes, 'time': alltimes, 'zlib': allzlib})
data.to_csv(os.path.join(pth, "dataframe_refzlib.csv"))
# system zlib: mean = 2.76 sec
# cfzlib     : mean = 2.14