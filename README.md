# zlib upgrade

Nearly all FSL tools default to writing gzipped files to save analysis outputs. This process can happen many times within a single operation such as BET. 

By upgrading the linked version of zlib we can gain a performance boost (faster I/O).

This project serves as a demonstration of expected performance gains. 

## Cloudflare zlib

The cloud flare zlib has been demonstrated to outperform the standard zlib library. By upgrading to this version of zlib, all FSL tools that link to zlib will get a performance boost. 

## Benchmarks

### FSLMATHS test case

- tests consisted of running the `fslmaths in.nii.gz -add 0.0 out.nii.gz` command

- input files were already gzipped (so and unzip + zip was required for I/O)

- 20 levels of file size were used

- Averaged across 20 repetitions for each level of file size

- cloudflare zlib and the system provided zlib were compared (same as compiled in FSL 6.0.1)

### BET test case

-  tests consisted of running the `bet in.nii.gz out.nii.gz -m -F` command.

- input files were already gzipped (so and unzip + zip was required for I/O)

- Averaged across 10 repetitions for each verison of BET (compiled with cfzlib and system zlib)
 
#### Hardware

- OS: macOS 10.14.3

- Machine: MacBook Pro (15-inch, 2018)

- Processor: 2.6 GHz Intel Core i7

- Memory: 16 GB 2400 MHz DDR4

- Graphics: Intel UHD Graphics 630 1536 MB

- Storage: Apple SSD

## Results

#### Cloudflare zlib version

|    	| file_size  	| time                	| zlib   	|
|----	|------------	|---------------------	|--------	|
| 0  	| 5.464614   	| 0.2207685112953186  	| cfzlib 	|
| 1  	| 10.929163  	| 0.42619708776473997 	| cfzlib 	|
| 2  	| 16.393645  	| 0.636983335018158   	| cfzlib 	|
| 3  	| 21.858194  	| 0.8377013683319092  	| cfzlib 	|
| 4  	| 27.323098  	| 1.0749923706054687  	| cfzlib 	|
| 5  	| 32.787829  	| 1.2852209329605102  	| cfzlib 	|
| 6  	| 38.252491  	| 1.4618040800094605  	| cfzlib 	|
| 7  	| 43.717299  	| 1.6468891739845275  	| cfzlib 	|
| 8  	| 49.182056  	| 1.8494682669639588  	| cfzlib 	|
| 9  	| 54.646841  	| 2.0469688892364504  	| cfzlib 	|
| 10 	| 60.111506  	| 2.2677374482154846  	| cfzlib 	|
| 11 	| 65.576269  	| 2.4356144309043883  	| cfzlib 	|
| 12 	| 71.041135  	| 2.6410425662994386  	| cfzlib 	|
| 13 	| 76.505811  	| 2.890670108795166   	| cfzlib 	|
| 14 	| 81.970515  	| 3.0112656712532044  	| cfzlib 	|
| 15 	| 87.435201  	| 3.2260849237442017  	| cfzlib 	|
| 16 	| 92.899915  	| 3.4418022632598877  	| cfzlib 	|
| 17 	| 98.364545  	| 3.6306890845298767  	| cfzlib 	|
| 18 	| 103.829131 	| 3.819137156009674   	| cfzlib 	|
| 19 	| 109.293625 	| 4.0478958010673525  	| cfzlib 	|

#### Reference zlib (system default)

|    	| file_size  	| time               	| zlib    	|
|----	|------------	|--------------------	|---------	|
| 0  	| 5.464614   	| 0.2808870553970337 	| refzlib 	|
| 1  	| 10.929163  	| 0.565443468093872  	| refzlib 	|
| 2  	| 16.393645  	| 0.8108768463134766 	| refzlib 	|
| 3  	| 21.858194  	| 1.0666980862617492 	| refzlib 	|
| 4  	| 27.323098  	| 1.3455530643463134 	| refzlib 	|
| 5  	| 32.787829  	| 1.6246402621269227 	| refzlib 	|
| 6  	| 38.252491  	| 1.883015787601471  	| refzlib 	|
| 7  	| 43.717299  	| 2.1501861810684204 	| refzlib 	|
| 8  	| 49.182056  	| 2.3712019443511965 	| refzlib 	|
| 9  	| 54.646841  	| 2.640587496757507  	| refzlib 	|
| 10 	| 60.111506  	| 2.889743435382843  	| refzlib 	|
| 11 	| 65.576269  	| 3.203321433067322  	| refzlib 	|
| 12 	| 71.041135  	| 3.456670045852661  	| refzlib 	|
| 13 	| 76.505811  	| 3.8162039160728454 	| refzlib 	|
| 14 	| 81.970515  	| 3.903820824623108  	| refzlib 	|
| 15 	| 87.435201  	| 4.273071241378784  	| refzlib 	|
| 16 	| 92.899915  	| 4.531004881858825  	| refzlib 	|
| 17 	| 98.364545  	| 4.758320164680481  	| refzlib 	|
| 18 	| 103.829131 	| 5.361556327342987  	| refzlib 	|
| 19 	| 109.293625 	| 5.214437508583069  	| refzlib 	|

# Plots

## fslmaths time by zlib version

![Time by file size](fig_results_line.png)

## Performance gains

![percent diff](fig_results_line_pctdiff.png)

## bet time by zlib version

![Time for bet](fig_results_bar.png)


