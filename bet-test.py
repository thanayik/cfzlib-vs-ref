import os
import sys
import time
from glob import glob
from statistics import mean
import pandas as pd

pth = "/Users/thanayik/Dev/cfzlib-tests"
print(pth)
file = os.path.join(pth, "T1.nii.gz")
out = os.path.join(pth, "out.nii.gz")
# files = glob(os.path.join(pth, "combo*.nii.gz"))
# files.sort()
reps = 10

alltimes = []
allsizes = []
allzlib = []

times = []
for i in range(0, reps):
    cmd = "bet {} {} -m -R".format(file, out)
    # print(cmd)
    start = time.time()
    os.system(cmd)
    dur = time.time() - start  # duration in secs
    # print("gzip took: {}".format(dur))
    times.append(dur)
    os.remove(out)


# print(alldurs)
print("time: {}".format(mean(times)))

# reference version (system)    time: 16.201822996139526
# cfzlib version                time: 13.429291462898254