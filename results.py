# import seaborn as sns
# sns.set()
import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
import seaborn as sns
sns.set()
import pandas as pd
import os
import numpy as np

cfz = pd.read_csv("dataframe_cfzlib.csv")
ref = pd.read_csv("dataframe_refzlib.csv")

data = pd.concat([cfz, ref])

x = np.linspace(0, 50, 200)  # Create a list of evenly-spaced numbers over the range
y = np.sin(x)

# plt.plot(x,y)
# plt.show()
ax = sns.lineplot(x="file_size", y="time", hue="zlib", style="zlib", markers=True, dashes=False, data=data)